
public class LetterCounter {

    private String word;

    public LetterCounter(String word)  {

        this.word = word;
    }

    public String letterSelector(int n)    {

        char[] letters = new char[word.length()];

        for (int i = n - 1; i < letters.length; i ++)    {

            if ( i == -2) {

                return "";

            }

            letters[i] = word.charAt(i);

            i = i + (n - 1);
        }

        String selected = "";

        for (int j = 0; j < letters.length ; j ++)   {

            if ( Character.isUpperCase(letters[j]) ) {

                selected = selected + letters[j] ;

            }

        }

        return selected;
    }
}
